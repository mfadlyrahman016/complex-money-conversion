﻿namespace ComplexConversion
{
    public class Program
    {
        static Dictionary<decimal, string> basicNumber = new Dictionary<decimal, string>()
        {
            {1 , "one"},
            {2 , "two"},
            {3 , "three"},
            {4 , "four"},
            {5 , "five"},
            {6 , "six"},
            {7 , "seven"},
            {8 , "eight"},
            {9 , "nine"},
            {10 , "ten"},
            {11 , "eleven"},
            {12 , "twelve"},
            {13 , "thirteen"},
            {14 , "fourteen"},
            {15 , "fifthteen"},
            {16 , "sixteen"},
            {17 , "seventeen"},
            {18 , "eighteen"},
            {19 , "nineteen"},
            {20 , "twenty"},
            {30 , "thirty"},
            {40 , "forty"},
            {50 , "fifty"},
            {60 , "sixty"},
            {70 , "seventy"},
            {80 , "eighty"},
            {90 , "ninety"},
            {100 , "hundred"},
            {1000 , "thousand"},
            {1000000 , "million"},
            {1000000000 , "billion"},
            {1000000000000 , "trillion"}
        };

        static void Main(string[] args)
        {
            //One hundred and twenty-seven dollars and forty-five cents
            //decimal input = 43.03M;
            //decimal input = 127.459M;
            //decimal input = 1340652500.459M;
            //decimal input = 2450300.459M;
            while(true)
            {
                Console.WriteLine();
                Console.Write("Input number : $ ");
                if (decimal.TryParse(Console.ReadLine(), out decimal input))
                {
                    Console.WriteLine(string.Concat("Conversion   : ", Process(input)));
                }
                else
                {
                    Console.WriteLine("Please input a number correctly !!!");
                }
            }
        }

        static string Process(decimal input)
        {
            var result = string.Empty;

            try
            {
                //dollars
                var dollars = string.Empty;
                input = Math.Truncate(input * 100) / 100;
                if ((long)input > 0)
                {
                    dollars = Translate(input).Trim();
                    dollars = char.ToUpper(dollars[0]) + dollars.Substring(1) + " dollars";
                }                

                //cents
                var cents = string.Format("{0:C}", input);
                cents = cents.Substring(cents.Length - 2);
                decimal.TryParse(cents, out decimal centsVal);

                if (centsVal > 0)
                    cents = Translate(centsVal).Trim() + " cents";
                else
                    cents = "";

                var isDollarCentExist = (long)input > 0 && centsVal > 0;
                result = string.Concat(dollars, isDollarCentExist ? " and " : "", cents);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        static string Translate(decimal number)
        {
            var result = string.Empty;

            try
            {
                foreach (var item in basicNumber.Reverse())
                {
                    var div = (long)number / (long)item.Key;

                    if (div > 0)
                    {
                        if (number < 13 || (div == 1 && (long)number == (long)item.Key))
                        {
                            result += (number >= 100 ? Translate(div) + " " : "") + item.Value;
                            break;
                        }
                        else if (number < 100)
                        {
                            result += item.Value + "-" + basicNumber[(long)number % (long)item.Key];
                            break;
                        }

                        var balance = number - (item.Key * div);
                        result += Translate(div) + " " + item.Value + (balance > 0 ? 
                                                                                (balance < 100 ? " " : " and ") + Translate(number - (item.Key * div)) 
                                                                       : "");
                        break;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}